# -*- coding: utf-8 -*-

# Define your item pipelines here
#
# Don't forget to add your pipeline to the ITEM_PIPELINES setting
# See: https://docs.scrapy.org/en/latest/topics/item-pipeline.html

import mysql.connector


class XsktPipeline:

    def __init__(self):
        self.create_connection()
        self.create_table()

    def create_connection(self):
        self.conn = mysql.connector.connect(
            host='localhost',
            user='root',
            database='xskt'
        )
        self.curr = self.conn.cursor()

    def create_table(self):
        self.curr.execute("""drop table if exists xskt""")
        self.curr.execute("""create table xskt(
                                Thu varchar(10),
                                Ngay_thang varchar(10),
                                Nam varchar(10),
                                kq text)""")

    def process_item(self, item, spider):
        self.store_db(item)
        return item

    def store_db(self, item):
        self.curr.execute("""insert into xskt values(%s,%s,%s,%s)""", (
            item['xs_info'][0],
            item['xs_info'][1],
            item['xs_info'][2],
            str(item['xs_data'])

        ))
        self.conn.commit()
