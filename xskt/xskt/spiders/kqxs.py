# -*- coding: utf-8 -*-
import scrapy
import calendar
import datetime
from ..items import XsktItem


def get_total_date_month(year, month):
    now = datetime.datetime.now()
    total_date = calendar.monthrange(year, month)[1]

    if year == now.year and month == now.month and now.day < total_date:
        return now.day

    return total_date


class KQXS_Spider(scrapy.Spider):
    name = 'kqxs'
    start_urls = []

    month_to_scrap = 2
    year_to_scrap = 2020

    total_date = get_total_date_month(year_to_scrap, month_to_scrap)
    for i in range(1, total_date):
        start_urls.append('https://xskt.com.vn/xsmn/ngay-{0}-{1}-{2}'.format(i, month_to_scrap, year_to_scrap))

    def parse(self, response):
        items = XsktItem()
        tmp_data = {}
        items['xs_info'] = [
            response.css("#MN0 th:nth-child(1) a::text").extract_first(),
            response.css("#MN0 th:nth-child(1)::text").extract_first(),
            self.year_to_scrap
        ]

        for i in range(2, 5):
            location = response.css("#MN0 th:nth-child({0}) a::text".format(i)).extract_first()
            tmp_data[location] = {}
            for j in range(2, 11):
                prize = response.css("#MN0 tr:nth-child({0}) td:nth-child(1)::text".format(j)).extract_first()
                number = response.css("#MN0 tr:nth-child({0}) td:nth-child({1})".format(j, i)).css("::text").extract()
                tmp_data[location][prize] = ', '.join(number)

        items['xs_data'] = tmp_data
        yield items





